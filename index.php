<?php
/**
 * File sharing system
 * 
 * @author Pierre HUBERT
 */

const STORAGE_PATH = __DIR__."/files/";
const STORAGE_URL = "http://devweb.local/fileshare/files/";
const EXPIRE_FILE_NAME = "expire_time";
const FILE_DURATION = 10;
const ALLOWED_MIME_TYPE = array(
	"image/png",
	"application/pdf",
	"application/zip",
	"image/webp",
	"application/x-rar-compressed",
	"application/x-7z-compressed",
	"image/jpeg",
	"image/gif",
	"application/epub+zip"
);

// If empty, the system will be open
const ALLOWED_IPs = array(
	
);

// https://stackoverflow.com/a/4356295/3781411
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

// https://stackoverflow.com/a/3349792/3781411
function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

// Check IP address (if required)
if(count(ALLOWED_IPs) != 0) {
	if(!in_array($_SERVER["REMOTE_ADDR"], ALLOWED_IPs))
		exit("Access denied !");
}

// Perform files cleanup
foreach(glob(STORAGE_PATH."*") as $dir) {
	$expire_file = $dir."/".EXPIRE_FILE_NAME;

	// Ignore files without expiration
	if(!file_exists($expire_file))
		continue;
	
	$expire_time = file_get_contents($expire_file);

	if($expire_time > time())
		continue;
	
	// Delete directory
	deleteDir($dir);
}

// Check if a file was sent
if(isset($_FILES["file"])) {

	if($_FILES["file"]["error"] != 0 || $_FILES["file"]["size"] == 0) {
		$error = "Error while uploading file!";
	}

	else if(!in_array($_FILES["file"]["type"], ALLOWED_MIME_TYPE)) {
		$error = "File format ".$_FILES["file"]["type"]." is not allowed!";
	}

	else if($_FILES["file"]["name"] == EXPIRE_FILE_NAME)
		$error = "File name is reserved!";

	else {
		$targetDIR = generateRandomString(10)."/";
		$targetPath = STORAGE_PATH.$targetDIR;
		$targetURL = STORAGE_URL.$targetDIR;
		$fileName = $_FILES["file"]["name"];
		$success = $targetURL;

		if(!mkdir($targetPath)) {
			$error = "Could not create target directory!";
		}
		else if(!file_put_contents($targetPath.EXPIRE_FILE_NAME, time() + FILE_DURATION)) {
			$error = "Could not create metadata file!";
		}
		else if(!move_uploaded_file($_FILES["file"]["tmp_name"], $targetPath.$fileName))
			$error = "Could not move created file!";
		else
			$success = $targetURL.$fileName;
	}

}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>FileShare</title>

	<link rel="stylesheet" href="style.css" />
</head>
<body>

	<form class="share_form" action="<?= $_SERVER["PHP_SELF"] ?>" method="post" enctype="multipart/form-data">
		<h1>FileShare</h1>

		<?php if(isset($error)) { ?>
		<p class="error">Error: <?= $error ?></p>
		<?php } ?>

		<?php if(isset($success)) { ?>
		<p class="success">Success: <?= $success ?></p>
		<?php } ?>

		<input type="file" name="file" id="file" />
		<input type="submit" value="Envoyer" />
	</form>

</body>
</html>